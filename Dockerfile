FROM ruby:latest

RUN curl -sL https://deb.nodesource.com/setup_4.x | bash -
RUN apt-get install -y nodejs

RUN gem install rails -v 5.0.0
RUN mkdir /opt/app
COPY Gemfile /opt/app
COPY Gemfile.lock /opt/app
WORKDIR /opt/app
RUN echo "hello world"
RUN bundle install
COPY . /opt/app

EXPOSE 3000